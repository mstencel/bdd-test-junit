package com.example;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "target/generated-test-resources/" }, snippets = SnippetType.CAMELCASE,
        format = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"})
public class CucumberIT {

}