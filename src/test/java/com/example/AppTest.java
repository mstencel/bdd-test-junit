package com.example;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    @Given("^there are some historical stories$")
    public void thereAreSomeHistoricalStories() throws Throwable {
    }

    @When("^user wants to display report$")
    public void userWantsToDisplayReport() throws Throwable {
    }

    @Then("^historical stories will be displayed in report$")
    public void historicalStoriesWillBeDisplayedInReport() throws Throwable {
    }

    @Given("^user has different email address$")
    public void userHasDifferentEmailAddress() throws Throwable {
    }

    @When("^admin wants to set a different email address$")
    public void adminWantsToSetADifferentEmailAddress() throws Throwable {
    }

    @When("^user already has mapping$")
    public void userAlreadyHasMapping() throws Throwable {
    }

    @Then("^override dialog appears$")
    public void overrideDialogAppears() throws Throwable {
    }

    @When("^user doesn't have mapping$")
    public void userDoesnTHaveMapping() throws Throwable {
    }

    @Then("^mapping is saved$")
    public void mappingIsSaved() throws Throwable {
    }


}
